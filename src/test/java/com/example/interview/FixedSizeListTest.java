package com.example.interview;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;


public class FixedSizeListTest {

    private String[] examples = {"test1", "test2", "test3"};
    private List<String> testingCollection;

    @Before
    public void setup(){
        testingCollection = new FixedSizeList<>(examples.length);
        Collections.addAll(testingCollection, examples);
    }

    @Test
    public void adding_new_element_while_collection_is_full_replaces_first_element_with_new_one(){
        String newElement = "test4";
        testingCollection.add(newElement);

        try{
            Thread.sleep(10000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        Assert.assertEquals(newElement, testingCollection.get(0));
        System.out.println(testingCollection.toString());
    }

    @Test
    public void checking_if_get_returns_null_if_element_was_added_up_to_10_seconds_ago(){

        try{
            Thread.sleep(10000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        String newElement = "test4";
        testingCollection.add(newElement);

        Assert.assertNull(testingCollection.get(0));
        System.out.println(testingCollection.toString());
    }
}
