package com.example.interview;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.AbstractList;

public class FixedSizeList<E> extends AbstractList<E> {

    private Object[] collectionElements;
    private final int capacity;
    private int nextIndex;
    private static final int NUMBER_OF_SECONDS = 10;

    public FixedSizeList(int capacity){
        this.capacity = capacity;
        collectionElements = new Object[capacity];
        nextIndex = 0;
    }

    @Override
    public boolean add(E element){
        if(nextIndex == capacity){
            collectionElements[0] = new CollectionElement(element);
        }else{
            collectionElements[nextIndex] = new CollectionElement(element);
            nextIndex++;
        }

        return true;
    }

    @Override
    public E get(int index) {
        if(index < 0 || index >= capacity) {
            throw new IndexOutOfBoundsException("Incorrect index! Please provide index from the following range: [0, " + capacity +"]");
        }

        CollectionElement collectionElement = (CollectionElement) collectionElements[index];
        if(collectionElement == null || Duration.between(collectionElement.timestamp, LocalDateTime.now()).getSeconds() < NUMBER_OF_SECONDS)
            return null;
        return collectionElement.element;
    }

    @Override
    public int size() {
        return collectionElements.length;
    }



    class CollectionElement {
        private E element;
        private LocalDateTime timestamp;

        CollectionElement(E element){
            this.element = element;
            this.timestamp = LocalDateTime.now();
        }

    }
}
